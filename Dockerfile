FROM python:3.11-slim-buster
ENV PYTHONUNBUFFERED 1
COPY --from=openjdk:11-jre-slim /usr/local/openjdk-11 /usr/local/openjdk-11
ARG PG_HOST
ARG PG_PORT
ARG PG_USER
ARG PG_PASSWORD
ARG PG_DBNAME
ARG PG_SCHEMA
ENV PYTHONUNBUFFERED 1
ENV JAVA_HOME /usr/local/openjdk-11
ENV PATH $PATH:$JAVA_HOME/bin
ENV PG_PORT $PG_PORT
ENV PG_USER $PG_USER
ENV PG_PASSWORD $PG_PASSWORD
ENV PG_DBNAME $PG_DBNAME
ENV PG_HOST $PG_HOST
ENV PG_SCHEMA $PG_SCHEMA

RUN apt-get update && \
    apt-get install -y wget && \
    wget -P /usr/local/bin/ https://github.com/ontodev/robot/releases/download/v1.9.1/robot.jar && \
    wget -P /usr/local/bin/ https://raw.githubusercontent.com/ontodev/robot/master/bin/robot && \
    wget -qO /usr/local/bin/dasel https://github.com/TomWright/dasel/releases/download/v2.1.2/dasel_linux_amd64 && \
    chmod +x /usr/local/bin/dasel && \
    chmod +x /usr/local/bin/robot
WORKDIR /chebi-ontology-generator
COPY . /chebi-ontology-generator

RUN pip install -r requeriments.txt && \
    chmod +x execute_robot.sh && \
    cp luigi.example.toml luigi.toml && \
    dasel put -f luigi.toml -t string -v '$PG_HOST' 'postgres.host' && \
    dasel put -f luigi.toml -t string -v '$PG_DBNAME' 'postgres.database' && \
    dasel put -f luigi.toml -t string -v '$PG_PORT' 'postgres.port' && \
    dasel put -f luigi.toml -t string -v '$PG_USER' 'postgres.user' && \
    dasel put -f luigi.toml -t string -v '$PG_PASSWORD' 'postgres.password' && \
    dasel put -f luigi.toml -t string -v '$PG_SCHEMA' 'postgres.schema'
ENTRYPOINT ["python", "/chebi-ontology-generator/ontology_pipeline.py", "CreateChEBIOntology", "--local-scheduler"]
