## What is this folder used for?

This folder has a subset of the full chebi ontology (`chebi.owl`). The idea is that we can use `chebi_for_testing.owl`
to perform testing operations. For example, to perform the testing process on the searching endpoints 
in [ChEBI Backend repository](https://gitlab.ebi.ac.uk/chembl/chebi/chebi-2.0/chebi-backend), we need to have testing data. There, we use `chebi_for_testing.owl` to perform text search
and advanced search.

## How `chebi_for_testing.owl` was created?

We use [robot tool](https://robot.obolibrary.org) to create a subset ontologically correct, preserving the hierarchy and relations among compounds 
defined in `chebi_subset.txt`. We highly recommend reading [robot extract documentation](https://robot.obolibrary.org/extract.html).
If for some reason you need to generate `chebi_for_testing.owl` again, please follow these steps (you need to have `robot` installed on your machine):

1. Download the last version of `chebi.owl`
2. Once you have `chebi.owl`: `cd /local/path/to/chebi.owl`
3. `robot extract --method BOT --input chebi.owl --term-file chebi_subset.txt --output chebi_for_testing.owl`
4. All object/annotation properties should be in the file, the above command just maintain those used by the 
compounds defined in `chebi_subset.txt`, so you must manually copy/paste all the object/annotation properties from `chebi.owl`.
5. Upload the new `chebi_for_testing.ow`l file in the `<FTP>/unit_testing/` path.

## Final notes
Once you have the `chebi_for_testing.owl` already created, you can start to play with it. You can, for example: indexing data on
Elasticsearch, using [pronto](https://pronto.readthedocs.io) to validate the ontology structure, opening it on [Protégé](https://protege.stanford.edu), etc.