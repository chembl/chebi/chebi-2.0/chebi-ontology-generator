#!/bin/bash

# We are getting the variants (full, core and lite). From the luigi task, we send the 5th parameter as follow:
# full;core;lite, so basically we create an array splitting the string by ';'.
# Based on https://stackoverflow.com/questions/918886/how-do-i-split-a-string-on-a-delimiter-in-bash
IFS=';' read -ra VARIANTS <<< "$5"
template_folder="$1"
current_date="$2"
release_version="$3"
ontology_folder="$4"


function rename_full_variant() {
  echo "Renaming ontology file's name in 'full' variant"
  mv "$ontology_folder"/chebi_full.owl "$ontology_folder"/chebi.owl &&
  mv "$ontology_folder"/chebi_full.obo "$ontology_folder"/chebi.obo &&
  mv "$ontology_folder"/chebi_full.json "$ontology_folder"/chebi.json &&
  mv "$ontology_folder"/chebi_full.owl.gz "$ontology_folder"/chebi.owl.gz &&
  mv "$ontology_folder"/chebi_full.obo.gz "$ontology_folder"/chebi.obo.gz &&
  mv "$ontology_folder"/chebi_full.json.gz "$ontology_folder"/chebi.json.gz
}

function execute_robot_report() {
  # Execute robot report against chebi_full.owl, chebi_full files will be renamed if robot report succeed.

  echo "Executing robot report for getting QA information"
  robot report \
    --input "$ontology_folder"/chebi_full.owl \
    --output "$template_folder"/chebi_report.tsv
}

function create_ontology_files() {
  for variant in "${VARIANTS[@]}"; do
    echo "Generating variant: $variant"
    echo "$template_folder/chebi_${variant}_robot_template.tsv"
    robot \
      template --template $template_folder/chebi_${variant}_robot_template.tsv \
                --prefix "chebi:http://purl.obolibrary.org/obo/chebi/" \
                --prefix "chemrof:https://w3id.org/chemrof/" \
                --ontology-iri "http://purl.obolibrary.org/obo/chebi.owl" \
      annotate --link-annotation dc:license https://creativecommons.org/licenses/by/4.0/ \
                --typed-annotation dc:title "ChEBI Ontology" xsd:string \
                --typed-annotation dc:description "Chemical Entities of Biological Interest, also known as ChEBI, is a chemical database and ontology of molecular entities focused on 'small' chemical compounds, that is part of the Open Biomedical Ontologies (OBO) effort at the European Bioinformatics Institute (EBI)." xsd:string \
                --annotation oboInOwl:saved-by "chebi" \
                --annotation oboInOwl:default-namespace "chebi_ontology" \
                --annotation oboInOwl:hasOBOFormatVersion 1.2 \
                --annotation oboInOwl:date "$current_date" \
                --typed-annotation owl:versionInfo "$release_version" xsd:decimal \
                --annotation rdfs:comment "ChEBI subsumes and replaces the Chemical Ontology first. This ontology was developed by Michael Ashburner and Pankaj Jaiswal. Data was cuarated by The ChEBI Curation Team. For any queries contact chebi-help@ebi.ac.uk" \
                --typed-annotation foaf:homepage "https://www.ebi.ac.uk/chebi" xsd:anyURI \
                --output "$ontology_folder"/chebi_${variant}.owl \
      convert --output "$ontology_folder"/chebi_${variant}.obo \
      convert --output "$ontology_folder"/chebi_${variant}.json && \
      gzip -c "$ontology_folder"/chebi_${variant}.obo > "$ontology_folder"/chebi_${variant}.obo.gz && \
      gzip -c "$ontology_folder"/chebi_${variant}.owl > "$ontology_folder"/chebi_${variant}.owl.gz && \
      gzip -c "$ontology_folder"/chebi_${variant}.json > "$ontology_folder"/chebi_${variant}.json.gz

      if [ "$variant" == "full" ]; then
        # The 'full' variant is the main variant of the ontology, so we execute robot report against the full ontology file
        # and then we rename them. If robot report fails, then we exit with status code 1. (error)
        if execute_robot_report; then
          rename_full_variant
        else
          echo "Robot report failed!"
          return 1
        fi
    fi

  done
}

create_ontology_files