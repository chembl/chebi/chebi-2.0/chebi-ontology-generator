#!/bin/bash

#SBATCH --time=2:00:00
#SBATCH --mem=10G
#SBATCH --mail-type=all
#SBATCH --output=<LOG_FILE>
#SBATCH --error=<LOG_FILE>
#SBATCH --job-name=<JOB_NAME>
#SBATCH --chdir=<CHEBI_SLURM_JOBS_HOME>
#SBATCH --export=SINGULARITY_DOCKER_USERNAME=<READ_DOCKER_USERNAME>,SINGULARITY_DOCKER_PASSWORD=<READ_DOCKER_TOKEN>


# Purge all modules, just in case
module purge

DOCKER_TAG=<DOCKER_TAG>

function execute_chebi_elasticsearch_indexation() {
    singularity pull -F chebi-elasticsearch.sif docker://dockerhub.ebi.ac.uk/chembl/chebi/chebi-2.0/chebi-elasticsearch:${DOCKER_TAG} && \
    singularity run chebi-elasticsearch.sif

    # Drop generated files: We don't delete .sh file and .out logs file
    # TODO: This won't be necessary once this issue is resolved: https://github.com/SvenMarcus/hpc-rocket/issues/52
    rm -rf $(pwd)/chebi-elasticsearch.sif
}

execute_chebi_elasticsearch_indexation