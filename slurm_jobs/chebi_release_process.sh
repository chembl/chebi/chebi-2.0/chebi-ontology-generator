#!/bin/bash

#SBATCH --time=2:00:00
#SBATCH --mem=32G
#SBATCH --mail-type=all
#SBATCH --output=<LOG_FILE>
#SBATCH --error=<LOG_FILE>
#SBATCH --chdir=<CHEBI_SLURM_JOBS_HOME>
#SBATCH --job-name=<JOB_NAME>
#SBATCH --partition=datamover
#SBATCH --export=SINGULARITY_DOCKER_USERNAME=<READ_DOCKER_USERNAME>,SINGULARITY_DOCKER_PASSWORD=<READ_DOCKER_TOKEN>


# Purge all modules, just in case
module purge

# General variables
FTP=<FTP>
ONTOLOGY_DOCKER_TAG=<ONTOLOGY_DOCKER_TAG>
DUMPS_DOCKER_TAG=<DUMPS_DOCKER_TAG>

# Variables used by chebi-ontology-generator container.
TMP_ONTOLOGY_FOLDER=$(pwd)/ontology_files # This is the folder where the chebi-ontology-generator image will save the generated ontology files, it will be used as a volume by Singularity.
TMP_TEMPLATES_FOLDER=$(pwd)/templates # This folder will store the robot template and the other 6 auxiliary templates, it is used as a volume by Singularity when the chebi-ontology-generator image is executed.

# Variables used by chebi-dumps container. Dumps related variables: SQL DUMPS, flat files and SDF files
TMP_SQL_DUMPS_FOLDER=$(pwd)/generic_dumps
TMP_SDF_DUMPS_FOLDER=$(pwd)/SDF
TMP_FLAT_FILES_FOLDER=$(pwd)/flat_files

#FTP related variables: Where the different files will be sent once they have been created in the above temporal folders
FTP_ONTOLOGY_FOLDER=${FTP}/ontology #This is the folder where the last version of the ontology is saved in the FTP
FTP_SQL_DUMPS_FOLDER=${FTP}/generic_dumps # This is the folder where the last version of the SQL dumps are saved in the FTP
FTP_SDF_DUMPS_FOLDER=${FTP}/SDF
FTP_FLAT_FILES_FOLDER=${FTP}/flat_files
FTP_ARCHIVE_FOLDER=${FTP}/archive # This folder is where all the past releases are saved.
OWNER='<REMOTE_USER>'
GROUP='chebi'

# Version variables
# Imagine you want to make a new release: 225, so:
# RELEASE_VERSION=225 (version you want to generate),
# CURRENT_RELEASE=224 (version got it from the .version file on the ChEBI public FTP)
RELEASE_VERSION=<RELEASE_VERSION>
CURRENT_RELEASE=$(cat ${FTP}/.version)


# 1. Archive the current version of the release. It means, save in: ${FTP}/archive all the data published the past month.
function archive_previous_version() {
    # Call this function with the root/to/folder/ you want to archive as an argument.
    # This is useful for archiving folders already published on the public FTP.

    if [ "$CURRENT_RELEASE" -ne "$RELEASE_VERSION" ]; then
      folder_to_archive=${1}
      mkdir -p ${FTP_ARCHIVE_FOLDER}/rel${CURRENT_RELEASE} && \
      rsync -a \
          --info=progress2 \
          --info=stats2,misc1,flist0 \
          ${folder_to_archive} ${FTP_ARCHIVE_FOLDER}/rel${CURRENT_RELEASE}
    else
      echo "Current release version is ($CURRENT_RELEASE), it is equal to the version you want to generate ($RELEASE_VERSION)."
      echo "You are running the process for the current release. Release $((${CURRENT_RELEASE} - 1)) has been already archived previously."
    fi
}


# 2. Once the current version of the release has been saved successfully, we can start the generation of the new ontology files, executing
# the chebi-ontology-generator image. We are running the ChEBI Ontology Generator using singularity to execute the docker image saved on the
# docker container registry. We are forcing to download (always) the image does not matter if the docker image is already
# saved locally in the cluster. The above is to asure that we are using always the last changes. Otherwise we would need to
# delete the image in the SLURM cluster manually each time we push changes on the gitlab repository. The singularity' image name is set to
# chebi_ontology_generator.sif.
# Something important to mention is that generated ontology files are saved temporally in ${TMP_ONTOLOGY_FOLDER} path
function execute_chebi_ontology_generator() {
    singularity pull -F chebi_ontology_generator.sif docker://dockerhub.ebi.ac.uk/chembl/chebi/chebi-2.0/chebi-ontology-generator:${ONTOLOGY_DOCKER_TAG} && \
    singularity run \
        -B ${TMP_TEMPLATES_FOLDER}:/chebi-ontology-generator/templates \
        -B ${TMP_ONTOLOGY_FOLDER}:/chebi-ontology-generator/ontology_files \
        --env ROBOT_JAVA_ARGS=-Xmx8G chebi_ontology_generator.sif --release-version=${RELEASE_VERSION}
}

# 3. Generate Dump files which are: SQL Dumps, SDF files and TSV flat files.
function execute_chebi_dump_generator() {
    mkdir -p  ${TMP_SQL_DUMPS_FOLDER} ${TMP_SDF_DUMPS_FOLDER} ${TMP_FLAT_FILES_FOLDER}

    singularity pull -F chebi_dumps.sif docker://dockerhub.ebi.ac.uk/chembl/chebi/chebi-2.0/chebi-dumps:${DUMPS_DOCKER_TAG} && \
    singularity run \
        -B ${TMP_SQL_DUMPS_FOLDER}:/chebi_dumps/generic_dumps \
        -B ${TMP_SDF_DUMPS_FOLDER}:/chebi_dumps/SDF \
        -B ${TMP_FLAT_FILES_FOLDER}:/chebi_dumps/flat_files \
        chebi_dumps.sif --release-version=${RELEASE_VERSION} --workers=3 --log-level INFO
}

# 4. Once files have been generated successfully, we proceed to publish them in the respective FTP folder with the new release.
function publish_files() {
    # Call the function with two arguments:
    # 1. The local folder containing all the files you intend to publish.
    # 2. The destination folder on the FTP server where they will be published.
    local_folder_to_publish=${1}
    ftp_destination_folder=${2}
    rsync -hrt \
    --info=progress2 \
    --info=stats2,misc1,flist0 \
    --chown=${OWNER}:${GROUP} \
    --chmod=ug+rw,Dg+s,Do+rx,Fo-w,Fo+r \
    --exclude="README.md" ${local_folder_to_publish}/ ${ftp_destination_folder}
}

# 5. With all the functions defined above, we create two workflows functions: First one to the ontology, second one to the dumps files.
function ontology_publication_workflow() {
    # Ontology release steps:
    # 1. Execute the ontology generator to get the .owl, .obo and .json files in the temporally folder.
    # 2. Archive the current ontology files published on the FTP
    # 3. Publish the new ones on the FTP
    echo "Executing ontology workflow"
    if execute_chebi_ontology_generator; then
        echo "========== Archiving Ontology v${CURRENT_RELEASE} =========="
        archive_previous_version ${FTP_ONTOLOGY_FOLDER}
        echo "========== Publishing Ontology v${RELEASE_VERSION} on FTP =========="
        publish_files ${TMP_ONTOLOGY_FOLDER} ${FTP_ONTOLOGY_FOLDER}

        # Drop generated files: We don't delete .sh file and .out logs file
        # TODO: This won't be necessary once this issue is resolved: https://github.com/SvenMarcus/hpc-rocket/issues/52
        rm -rf ${TMP_ONTOLOGY_FOLDER} ${TMP_TEMPLATES_FOLDER} chebi_ontology_generator.sif
    else
        echo "Error in ontology_publication_workflow"
        return 1
    fi
}

function dumps_publication_workflow() {
    # Dumps release steps:
    # 1. Execute the dumps generator to get the sql dumps, sdf files and flat files saved in their respective temporally folders.
    # 2. Archive the current dumps files published on the FTP
    # 3. Publish the new ones on the FTP
    echo "Executing dumps_publication_workflow"
    if execute_chebi_dump_generator; then
        echo "========== Archiving SQL Dumps v${CURRENT_RELEASE} =========="
        archive_previous_version ${FTP_SQL_DUMPS_FOLDER}
        echo "========== Archiving SDF Dumps v${CURRENT_RELEASE} =========="
        archive_previous_version ${FTP_SDF_DUMPS_FOLDER}
        echo "========== Archiving Flat Files v${CURRENT_RELEASE} =========="
        archive_previous_version ${FTP_FLAT_FILES_FOLDER}

        echo "========== Publishing SQL Dumps v${RELEASE_VERSION} on FTP =========="
        publish_files ${TMP_SQL_DUMPS_FOLDER} ${FTP_SQL_DUMPS_FOLDER}
        echo "========== Publishing SDF Dumps v${RELEASE_VERSION} on FTP =========="
        publish_files ${TMP_SDF_DUMPS_FOLDER} ${FTP_SDF_DUMPS_FOLDER}
        echo "========== Publishing Flat Files v${RELEASE_VERSION} on FTP =========="
        publish_files ${TMP_FLAT_FILES_FOLDER} ${FTP_FLAT_FILES_FOLDER}

        # Drop generated files: We don't delete .sh file and .out logs file
        # TODO: This won't be necessary once this issue is resolved: https://github.com/SvenMarcus/hpc-rocket/issues/52
        rm -rf ${TMP_SQL_DUMPS_FOLDER} ${TMP_SDF_DUMPS_FOLDER} ${TMP_FLAT_FILES_FOLDER} chebi_dumps.sif
    else
        echo "Error in dumps_publication_workflow"
        return 1
    fi
}

# We have two different 'run' functions. The first one to execute the release monthly, which include all the steps
# described previously. And the second one is to execute the daily update in FTP/ontology/nightly
function run_monthly_release() {
    if <WORKFLOW>; then
      echo "<WORKFLOW> finished successfully"
    else
      return 1
    fi
}


function run_daily_update() {
    if execute_chebi_ontology_generator; then
      # Ontology is updated daily in a different path. This folder is to update the ontology nightly.
      FTP_ONTOLOGY_FOLDER=${FTP}/ontology/nightly
      # In this case we don't archive anything, for the above we can't call ontology_publication_workflow function here.
      publish_files ${TMP_ONTOLOGY_FOLDER} ${FTP_ONTOLOGY_FOLDER}

      # Drop generated files: We don't delete .sh file and .out logs file
      # TODO: This won't be necessary once this issue is resolved: https://github.com/SvenMarcus/hpc-rocket/issues/52
      rm -rf ${TMP_ONTOLOGY_FOLDER} ${TMP_TEMPLATES_FOLDER} chebi_ontology_generator.sif
    else
      return 1
    fi
}

<RUN_FUNCTION>
