#!/bin/bash

#SBATCH --time=2:00:00
#SBATCH --mem=16G
#SBATCH --mail-type=all
#SBATCH --output=<LOG_FILE>
#SBATCH --error=<LOG_FILE>
#SBATCH --chdir=<CHEBI_SLURM_JOBS_HOME>
#SBATCH --job-name=<JOB_NAME>
#SBATCH --partition=datamover
#SBATCH --export=SINGULARITY_DOCKER_USERNAME=<READ_DOCKER_USERNAME>,SINGULARITY_DOCKER_PASSWORD=<READ_DOCKER_TOKEN>,DBT_PROJECT_DIR=/chebi_data_management,DBT_PROFILES_DIR=/chebi_data_management


# Purge all modules, just in case
module purge

# Variables used by chebi-data-management container.
TMP_DBT_LOGS_FOLDER=$(pwd)/logs
TMP_DBT_TARGET_FOLDER=$(pwd)/target
DOCKER_TAG=<DOCKER_TAG>
MODELS_TO_SYNC="<MODELS_TO_SYNC>"

# This function execute the process to synchronize the schemas needed to generate the ontology and the DUMPS.
# We're utilizing an ELT framework called dbt (https://docs.getdbt.com), which dynamically creates configuration folders during execution.
# However, since files within a singularity container are read-only, new folders/files can't be created without assigning a volume.
# To address this, we need to create three "dbt" folders to serve as "volume bind mounts" in the singularity container.
function execute_chebi_data_management() {
    mkdir -p ${TMP_DBT_LOGS_FOLDER} ${TMP_DBT_TARGET_FOLDER}

    singularity pull -F chebi_data_management.sif docker://dockerhub.ebi.ac.uk/chembl/chebi/chebi-2.0/chebi-data-management:${DOCKER_TAG} && \
    singularity run \
        -B ${TMP_DBT_LOGS_FOLDER}:/chebi_data_management/logs \
        -B ${TMP_DBT_TARGET_FOLDER}:/chebi_data_management/target \
        chebi_data_management.sif dbt build --select "$MODELS_TO_SYNC" --target postgres

    # Drop generated files: Ww don't delete .sh file and .out logs file
    # TODO: This won't be necessary once this issue is resolved: https://github.com/SvenMarcus/hpc-rocket/issues/52
    rm -rf ${TMP_DBT_LOGS_FOLDER} ${TMP_DBT_TARGET_FOLDER} chebi_data_management.sif
}

execute_chebi_data_management