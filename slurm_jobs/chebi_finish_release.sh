#!/bin/bash

#SBATCH --time=2:00:00
#SBATCH --mem=1G
#SBATCH --mail-type=all
#SBATCH --output=<LOG_FILE>
#SBATCH --error=<LOG_FILE>
#SBATCH --chdir=<CHEBI_SLURM_JOBS_HOME>
#SBATCH --job-name=<JOB_NAME>
#SBATCH --partition=datamover

# Purge all modules, just in case
module purge

# General variables
FTP=<FTP>

# This function updates the file .version in the FTP with the new release version and it will be executed
# if the run_monthly_release fished well.
function update_release_version() {
  cp $(pwd)/.version ${FTP}/.version &&
  singularity cache clean -f
  echo "========= Release was finished successfully ========="

  # Drop generated files: We don't delete .sh file and .out logs file
  # TODO: This won't be necessary once this issue is resolved: https://github.com/SvenMarcus/hpc-rocket/issues/52
  rm -rf $(pwd)/.version
}

update_release_version