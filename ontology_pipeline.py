import enum
import logging
import luigi
import numpy as np
import pandas as pd
import time

from datetime import datetime
from helper_functions.database_connection import (
    get_pg_connection,
)
from helper_functions.sql_functions import get_sql_sentence
from helper_functions.templates_functions import (
    delete_all_templates,
    TEMPLATES_FOLDER,
    OWL_FOLDER,
    FTP_DOCUMENTATION_FOLDER,
    parent_folder,
    read_file,
    write_file,
)
from luigi.contrib.external_program import (
    ExternalProgramTask,
)
from sqlalchemy import text
from typing import NamedTuple

logger = logging.getLogger("luigi-interface")


class TemplateHeader(NamedTuple):
    """A named tuple to save the information about the robot template headers"""

    column_name: str  # This must match with the name columns that we have in the different sql sentences. human-readable, nothing special
    robot_command: str  # This is the robot special command associated to the column_name.


class OntologyVariants(enum.StrEnum):
    FULL = enum.auto()
    CORE = enum.auto()
    LITE = enum.auto()


header_robot_template = (
    TemplateHeader("curie", "ID"),  # What is a curie?: https://cthoyt.com/2021/09/14/curies.html
    TemplateHeader("property_type", "TYPE"),
    TemplateHeader("characteristic", "CHARACTERISTIC"),
    TemplateHeader("label", "A rdfs:label"),
    TemplateHeader("sub_property", "SP %"),
    TemplateHeader("inverse_of", "IP %"),
    TemplateHeader("comment", "A rdfs:comment"),
    TemplateHeader("xref", "A oboInOwl:hasDbXref"),
    TemplateHeader("namespace", "A oboInOwl:hasOBONamespace"),
    TemplateHeader("object_property", "A oboInOwl:id"),
    TemplateHeader("is_cyclic", "AT oboInOwl:is_cyclic^^xsd:boolean"),
    TemplateHeader("is_transitive", "AT oboInOwl:is_transitive^^xsd:boolean"),
    TemplateHeader("compounds_functional_parent", "SC 'has functional parent' some % SPLIT=|"),
    TemplateHeader("compounds_parent_hydride", "SC 'has parent hydride' some % SPLIT=|"),
    TemplateHeader("compounds_has_part", "SC 'has part' some % SPLIT=|"),
    TemplateHeader("compounds_has_role", "SC 'has role' some % SPLIT=|"),
    TemplateHeader("compounds_parents", "SC % SPLIT=|"),
    TemplateHeader("compounds_conjugate_acid_of", "SC 'is conjugate acid of' some % SPLIT=|"),
    TemplateHeader("compounds_conjugate_base_of", "SC 'is conjugate base of' some % SPLIT=|"),
    TemplateHeader("compounds_enantiomers_of", "SC 'is enantiomer of' some % SPLIT=|"),
    TemplateHeader("compounds_is_substituent_group_from", "SC 'is substituent group from' some % SPLIT=|"),
    TemplateHeader("compounds_is_tautomer_of", "SC 'is tautomer of' some % SPLIT=|"),
    TemplateHeader("description", "A obo:IAO_0000115"),
    TemplateHeader("charge", "AT chemrof:charge^^xsd:integer"),
    TemplateHeader("formula", "A chemrof:generalized_empirical_formula"),
    TemplateHeader("inchi", "A chemrof:inchi_string"),
    TemplateHeader("inchikey", "A chemrof:inchi_key_string"),
    TemplateHeader("wurcs", "A chemrof:wurcs_representation"),
    TemplateHeader("mass", "AT chemrof:mass^^xsd:decimal"),
    TemplateHeader("monoisotopic_mass", "AT chemrof:monoisotopic_mass^^xsd:decimal"),
    TemplateHeader("smiles", "A chemrof:smiles_string"),
    TemplateHeader("alternative_ids", "A oboInOwl:hasAlternativeId SPLIT=|"),
    TemplateHeader("chebi_id", "A oboInOwl:id"),
    TemplateHeader("stars", "AI oboInOwl:inSubset"),
    TemplateHeader("database_references", "A oboInOwl:hasDbXref SPLIT=|"),
    TemplateHeader("axioms_database_references", ">A oboInOwl:source SPLIT=|"),
    TemplateHeader("exact_synonym", "A oboInOwl:hasExactSynonym SPLIT=|"),
    TemplateHeader("axioms_exact_synonym", ">A oboInOwl:hasDbXref"),
    TemplateHeader("axioms_exact_synonym_type", ">AI oboInOwl:hasSynonymType"),
    TemplateHeader("related_synonym", "A oboInOwl:hasRelatedSynonym SPLIT=|"),
    TemplateHeader("axioms_related_synonym", ">A oboInOwl:hasDbXref"),
    TemplateHeader("axioms_related_synonym_type", ">AI oboInOwl:hasSynonymType"),
    TemplateHeader("is_deprecated", "AT owl:deprecated^^xsd:boolean"),
    TemplateHeader("terms_merged_definition", "AI obo:IAO_0000231"),
    TemplateHeader("terms_merged_with", "AI obo:IAO_0100001"),
)


class ExtractCompoundInformation(luigi.Task):
    def output(self):
        return {
            "base_information": luigi.LocalTarget(TEMPLATES_FOLDER / "base_information.tsv"),
            "axioms_database_references": luigi.LocalTarget(TEMPLATES_FOLDER / "axioms_database_references.tsv"),
            "axioms_related_synonyms": luigi.LocalTarget(TEMPLATES_FOLDER / "axioms_related_synonyms.tsv"),
            "axioms_exact_synonyms": luigi.LocalTarget(TEMPLATES_FOLDER / "axioms_exact_synonyms.tsv"),
            "deprecated_compounds": luigi.LocalTarget(TEMPLATES_FOLDER / "deprecated_compounds.tsv"),
            "ontology_properties": luigi.LocalTarget(TEMPLATES_FOLDER / "ontology_properties.tsv"),
        }

    def run(self):
        with get_pg_connection().connect() as connection:
            # Generate the TSV files for the next step: compounds_information.tsv and the axioms
            for id_information in self.output().keys():
                sql_sentence = get_sql_sentence(f"{id_information}.sql")
                pd.read_sql_query(sql=text(sql_sentence), con=connection).to_csv(
                    sep="\t",
                    path_or_buf=self.output().get(id_information).path,
                    index=False,
                )


class CreateRobotTemplate(luigi.Task):
    def requires(self):
        return ExtractCompoundInformation()

    def output(self):
        return {variant: luigi.LocalTarget(TEMPLATES_FOLDER / f"chebi_{variant}_robot_template.tsv") for variant in OntologyVariants}

    def run(self):
        # 1. Set the ontology_properties dataframe.
        ontology_properties = pd.read_csv(
            self.input().get("ontology_properties").path,
            sep="\t",
        )
        # 2. Set the different parts of the compounds' information: Base information, database references axioms,
        # exact synonyms axioms and related synonyms axioms.
        compound_information_parts = [
            pd.read_csv(local_target.path, sep="\t")
            for id_information, local_target in self.input().items()
            if id_information != "ontology_properties"
        ]

        # 3. Assemble all the parts in one dataframe and order by 'compound_id'
        compound_information = pd.concat(compound_information_parts).set_index("compound_id").sort_index()

        # 4. Concat the ontology_properties with the complete information compound, then convert boolean and integer
        # columns to integer. Although this process was done in the sql files (for example,the ontology_properties.sql),
        # the pd.concat function turns them into floats. Robot tool is able to understand true/false and 1/0 as
        # boolean values, however 1.0 and 0.0 are not recognized as booleans. Same for column charge, which is integer,
        # but the pd.concat method turns it into float.
        chebi_robot_template = pd.concat(
            [ontology_properties, compound_information],
            ignore_index=True,
        )
        chebi_robot_template["is_cyclic"] = chebi_robot_template["is_cyclic"].astype("Int8")
        chebi_robot_template["is_transitive"] = chebi_robot_template["is_transitive"].astype("Int8")
        chebi_robot_template["is_deprecated"] = chebi_robot_template["is_deprecated"].astype("Int8")
        chebi_robot_template["charge"] = chebi_robot_template["charge"].astype("Int32")

        # 5. set NaN for duplicated descriptions (these cases should be resolved for the curators...)
        mask_duplicated_definitions = chebi_robot_template.duplicated(subset=["description"], keep=False)
        chebi_robot_template.loc[mask_duplicated_definitions, "description"] = np.nan

        # 6. Re-arrange the columns taking into account the order in the header_robot_template tuple and set
        # the columns of the dataframe with the special headers of the robot tool.
        chebi_robot_template = chebi_robot_template[[headers.column_name for headers in header_robot_template]]
        chebi_robot_template.columns = [
            list(chebi_robot_template.columns),
            list(headers.robot_command for headers in header_robot_template),
        ]

        # 7. Create the chebi_robot_template.tsv file which is the full template.
        chebi_robot_template.to_csv(
            sep="\t",
            path_or_buf=self.output().get("full").path,
            index=False,
        )

        # 8. We create the other two template's variants: chebi_core_robot_template.tsv and
        # chebi_lite_robot_template.tsv: - chebi_core_robot_template.tsv has all the columns except those related to
        # synonyms and database references. - chebi_lite_robot_template.tsv has all the columns except those related
        # to synonyms, database_references and chemical data information. Additionally, we need to drop the residual
        # rows with empty values in all the columns to each compound, as we delete columns, we will have rows with
        # information only in 'curie' column, and the rest with empty information. Remember, in the FULL robot
        # template we have many rows for different kind of information.
        chebi_core_robot_template = chebi_robot_template.drop(
            self.chebi_core_columns_not_include(),
            axis=1,
            level=0,
        )
        chebi_core_robot_template = chebi_core_robot_template.dropna(
            how="all",
            axis=0,
            subset=list(col for col in chebi_core_robot_template.columns.to_list() if col != ("curie", "ID")),
        )
        chebi_core_robot_template.to_csv(
            sep="\t",
            path_or_buf=self.output().get("core").path,
            index=False,
        )

        chebi_lite_robot_template = chebi_core_robot_template.drop(
            self.chebi_lite_columns_not_include(),
            axis=1,
            level=0,
        )
        chebi_lite_robot_template = chebi_lite_robot_template.dropna(
            how="all",
            axis=0,
            subset=list(col for col in chebi_lite_robot_template.columns.to_list() if col != ("curie", "ID")),
        )
        chebi_lite_robot_template.to_csv(
            sep="\t",
            path_or_buf=self.output().get("lite").path,
            index=False,
        )

    @classmethod
    def chebi_core_columns_not_include(cls):
        return [
            "database_references",
            "axioms_database_references",
            "exact_synonym",
            "axioms_exact_synonym",
            "axioms_exact_synonym_type",
            "related_synonym",
            "axioms_related_synonym",
            "axioms_related_synonym_type",
        ]

    @classmethod
    def chebi_lite_columns_not_include(cls):
        return cls.chebi_core_columns_not_include() + [
            "charge",
            "formula",
            "inchi",
            "inchikey",
            "mass",
            "monoisotopic_mass",
            "smiles",
        ]


class CreateChEBIOntology(ExternalProgramTask):
    template_folder = luigi.PathParameter(default=TEMPLATES_FOLDER)
    date = luigi.DateMinuteParameter(default=datetime.now())
    release_version = luigi.IntParameter()
    ontology_folder = luigi.PathParameter(default=OWL_FOLDER)
    variants = luigi.EnumListParameter(
        enum=OntologyVariants,
        default=tuple(variant for variant in OntologyVariants),
    )

    def requires(self):
        return CreateRobotTemplate()

    def program_args(self):
        # First argument of this list is the path to the shell script,
        # The next arguments are the parameters used by the shell script.
        # The last argument is the variants will be generated, it is separated by ; 'full;core;lite'
        return [
            f'{parent_folder / "execute_robot.sh"}',
            self.template_folder,
            self.date.strftime("%d:%m:%Y %H:%M"),
            self.release_version,
            self.ontology_folder,
            ";".join(tuple(v.value for v in self.variants)),
        ]

    def run(self):
        try:
            super().run()
            self.create_documentation()
            logger.info("> Pipeline finished successfully")
        finally:
            logger.info("> Deleting .tsv templates")
            delete_all_templates()

    def create_documentation(self) -> None:
        """Create README and LICENSE files in <FTP>/ontology folder"""
        readme = read_file(FTP_DOCUMENTATION_FOLDER / "chebi_ontology_ftp_doc.tmpl")
        readme = readme.replace(
            "{{ release_version }}",
            str(self.release_version),
        )
        readme = readme.replace(
            "{{ release_date }}",
            self.date.strftime("%Y-%m-%d"),
        )
        license = read_file(FTP_DOCUMENTATION_FOLDER / "LICENSE")
        write_file(OWL_FOLDER / "README", readme)
        write_file(OWL_FOLDER / "LICENSE", license)


if __name__ == "__main__":
    start_time = time.time()
    luigi_run_result = luigi.run(detailed_summary=True)
    logger.info(f"Status: {luigi_run_result.status}")
    # We are validating here that the execution status was succeeded.
    # Assertion helps us to return a proper exit status
    # 0 if all is OK, and > 0 if something is broken.
    assert luigi_run_result.status in [
        luigi.execution_summary.LuigiStatusCode.SUCCESS,
        luigi.execution_summary.LuigiStatusCode.SUCCESS_WITH_RETRY,
    ]
    logger.info(f"> ETL process successfully finished, execution time: {time.time() - start_time}")
