import envtoml
import sqlalchemy
from helper_functions.templates_functions import (
    parent_folder,
)

luigi_config = envtoml.load(open(f"{parent_folder}/luigi.toml"))


def get_pg_connection() -> sqlalchemy.Engine:
    configuration_database_pg = luigi_config.get("postgres")
    user = configuration_database_pg.get("user")
    password = configuration_database_pg.get("password")
    host = configuration_database_pg.get("host")
    port = configuration_database_pg.get("port")
    db_name = configuration_database_pg.get("database")
    schema = configuration_database_pg.get("schema")
    return sqlalchemy.create_engine(
        f"postgresql://{user}:{password}@{host}:{port}/{db_name}",
        connect_args={"options": f"-csearch_path={schema}"},
    )
