from pathlib import Path

parent_folder = Path(__file__).parents[1]
TEMPLATES_FOLDER = parent_folder / "templates"
OWL_FOLDER = parent_folder / "ontology_files"
FTP_DOCUMENTATION_FOLDER = parent_folder / "ftp_documentation"


def delete_template(templates: tuple[str, ...]):
    for template in templates:
        if Path(TEMPLATES_FOLDER / template).exists():
            Path(TEMPLATES_FOLDER / template).unlink()


def delete_all_templates() -> None:
    for f in TEMPLATES_FOLDER.glob("*.tsv"):
        f.unlink()


def write_file(file_path: Path, content: str) -> None:
    with open(file_path, mode="w") as f:
        f.write(content)


def read_file(file_path: Path) -> str:
    with open(file_path, mode="r") as f:
        return f.read()
