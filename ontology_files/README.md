## What is this folder used for?

This folder is used to store locally the ontology file (`chebi.owl`), and all its variants (e.g. `chebi-lite.owl`). If
you are running the pipeline using Docker, then this folder is used as a volume bind mount to generate the files.