SELECT
    aes.compound_id,
    aes.curie,
    aes.property_type,
    aes.exact_synonym,
    aes.axiom AS axioms_exact_synonym,
    aes.axiom_type AS axioms_exact_synonym_type
FROM chebi_ontology.axioms_exact_synonyms AS aes;
