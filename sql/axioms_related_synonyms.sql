SELECT
    aes.compound_id,
    aes.curie,
    aes.property_type,
    aes.related_synonym,
    aes.axiom AS axioms_related_synonym,
    aes.axiom_type AS axioms_related_synonym_type
FROM chebi_ontology.axioms_related_synonyms AS aes;
