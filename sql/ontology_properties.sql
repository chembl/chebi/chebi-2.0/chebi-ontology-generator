SELECT
    curie,
    property_type,
    characteristic,
    label,
    sub_property,
    inverse_of,
    comment,
    xref,
    namespace,
    object_property,
    is_cyclic,
    is_transitive
FROM chebi_ontology.ontology_properties;
