SELECT
    dc.compound_id,
    dc.curie,
    dc.property_type,
    dc.is_deprecated,
    dc.terms_merged_with,
    dc.terms_merged_definition,
    dc.namespace,
    dc.object_property
FROM chebi_ontology.deprecated_compounds AS dc;
