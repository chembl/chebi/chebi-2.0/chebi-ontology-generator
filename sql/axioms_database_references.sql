SELECT
    adr.compound_id,
    adr.curie,
    adr.property_type,
    adr.database_references,
    adr.axiom AS axioms_database_references
FROM chebi_ontology.axioms_database_references AS adr;
