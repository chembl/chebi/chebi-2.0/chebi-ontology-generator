## What is this folder used for?

This folder is used to store the different temporal `.tsv` files based on the result of the queries in the sql folder.
These temporal `.tsv` files would be transformed to obtain the final robot template. In the final step of the Datapipeline,
the `.tsv` files are deleted.